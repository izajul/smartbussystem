package com.example.smartbussystem.customers.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartbussystem.R;
import com.example.smartbussystem.customers.Adapter.BusSeatsPlanRecyclerViewAdapter;

public class BookSeatActivity_customers extends AppCompatActivity {

    private RecyclerView mBusSeatPlanRCV;
    private BusSeatsPlanRecyclerViewAdapter seatsPlanRecyclerViewAdapter;
    private Drawable booked_seat_drawable,not_booked_seat_drawable,selected_seat_drawable;
    private int mSeatBookingCounter = 0;
    String[] getway = { "bKash", "Rocket", "Ucash", "Credit Card", "Debit Card"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_seat_customers);

        booked_seat_drawable = getResources().getDrawable(R.drawable.seat_black);
        not_booked_seat_drawable = getResources().getDrawable(R.drawable.seat_gray);
        selected_seat_drawable = getResources().getDrawable(R.drawable.seat_green);
        mBusSeatPlanRCV = findViewById(R.id.bus_seat_plan_RCV);

        sutupSeats();
    }

    private void sutupSeats() {
        final RelativeLayout payment = findViewById(R.id.payment_option);
        final RelativeLayout seatplan = findViewById(R.id.seat_list);

        Spinner spinner = payment.findViewById(R.id.spinner1);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,getway);
        spinner.setAdapter(adapter);


        final RecyclerView recycler_spinner = findViewById(R.id.bus_seat_plan_RCV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recycler_spinner.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        recycler_spinner.setLayoutManager(layoutManager);
        seatsPlanRecyclerViewAdapter = new BusSeatsPlanRecyclerViewAdapter(this,
                new BusSeatsPlanRecyclerViewAdapter.SeatClickListener() {
                    @Override
                    public void onClickSeat(TextView tv, ImageView iv, int position) {
                        Log.d("IV-Tag:",""+selected_seat_drawable);
                        Log.d("IV-Tag:",""+not_booked_seat_drawable);
                        Log.d("IV-Tag:",""+booked_seat_drawable);
                        Log.d("IV-Tag:",""+iv.getTag());
                        if(iv.getTag()!=null) {
                            if(iv.getTag()==not_booked_seat_drawable){
                                iv.setImageDrawable(selected_seat_drawable);
                                iv.setTag(selected_seat_drawable);
                                mSeatBookingCounter++;
                            }else if (iv.getTag()==selected_seat_drawable){
                                iv.setImageDrawable(not_booked_seat_drawable);
                                iv.setTag(not_booked_seat_drawable);
                                mSeatBookingCounter--;
                            }else{
                                Toast.makeText(BookSeatActivity_customers.this, "Seat Already Booked", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                },not_booked_seat_drawable,booked_seat_drawable);
        recycler_spinner.setAdapter(seatsPlanRecyclerViewAdapter);

        ImageView back =  findViewById(R.id.back_from_seat_plan_ImgBTN);
        Button seat_book_BTN =  findViewById(R.id.seat_book_BTN);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (payment.getVisibility() == View.VISIBLE){
                    seatplan.setVisibility(View.VISIBLE);
                    payment.setVisibility(View.GONE);
                }else finish();
            }
        });
        seat_book_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bookbtn:","clicking Book BTN");
                if(mSeatBookingCounter>0){
                    seatplan.setVisibility(View.GONE);
                    payment.setVisibility(View.VISIBLE);

                }else{
                    Toast.makeText(BookSeatActivity_customers.this, "Select A Seat First!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
