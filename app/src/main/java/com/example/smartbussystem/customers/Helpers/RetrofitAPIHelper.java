package com.example.smartbussystem.customers.Helpers;

import android.util.Log;

import com.example.smartbussystem.BingDirectionApi.BingDirectionApi;
import com.example.smartbussystem.BingDirectionApi.Line;
import com.example.smartbussystem.customers.HttpConnector.ApiKeyContainer;
import com.example.smartbussystem.customers.modelsAPI.BusStopsModelAPI;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitAPIHelper {
    private Retrofit mBingRetrofit,mBongoRetrofit;

    public RetrofitAPIHelper() {
        //initializing mBingRetrofit
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();


        mBingRetrofit = new Retrofit.Builder()
                //.baseUrl("https://www.mapquestapi.com/directions/v2/")
                .baseUrl("http://dev.virtualearth.net/REST/V1/Routes/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mBongoRetrofit = new Retrofit.Builder()
                .baseUrl("http://bongobus.showcaseonline.net/api/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public void getBusById(int id,final RetrofitAPICallBack rc){
        Log.d("Busss","Inside getBusById");
        ApiKeyContainer apiKeyContainer = mBongoRetrofit.create(ApiKeyContainer.class);
        apiKeyContainer.getBusById(id).enqueue(new Callback<Map>() {
            @Override
            public void onResponse(Call<Map> call, Response<Map> response) {
                if (response.isSuccessful()){
                    Log.d("Busss","success"+response.body().toString());
                    Map bus = response.body();
                    rc.onGetBusById(bus);

                }
            }

            @Override
            public void onFailure(Call<Map> call, Throwable t) {
                Log.e("Busss","Fail");
            }
        });
    }

    public void getBusStoppage(final CharSequence ch, final RetrofitAPICallBack rc){
        ApiKeyContainer apiKeyContainer = mBongoRetrofit.create(ApiKeyContainer.class);
        apiKeyContainer.getStoppage(ch.toString()).enqueue(new Callback<BusStopsModelAPI>() {
            @Override
            public void onResponse(Call<BusStopsModelAPI> call, Response<BusStopsModelAPI> response) {
               // Log.i("BusStopsModelAPI","ch:"+ch.toString()+",success:"+new Gson().toJson(response.body()));
                try{
                    List<BusStopsModelAPI.Data> data = response.body().getData();
                    rc.onGetBusStoppages(data);
                 //   Log.i("BusStopsModelAPI","success:"+data.size());
                }catch (Exception e){
                    Log.e("BusStopsModelAPI","Exception:"+e);
                }
            }

            @Override
            public void onFailure(Call<BusStopsModelAPI> call, Throwable t) {
                Log.e("BusStopsModelAPI","Fail:"+t+", Call:"+call);
            }
        });
    }


    public void getDirectionByBing(LatLng to, LatLng from, final RetrofitAPICallBack rc){
        ApiKeyContainer apiKeyContainer = mBingRetrofit.create(ApiKeyContainer.class);
        String url = "Driving?wp.0="+from.latitude+","+from.longitude+
                "&wp.1="+to.latitude+","+to.longitude+
                "&optmz=distance&routeAttributes=routePath&output=json" +
                "&key=Ag6W3Fyl0ryTKLLywubmktVHXygWTQpLAXEhpTp1ZtUZGtIPmS49UZW0bTO_G3mI";
        //from bing
        Log.i("mBingRetrofit","in function url="+url);
        apiKeyContainer.getRoutesBingMap(url).enqueue(new Callback<BingDirectionApi>() {
            @Override
            public void onResponse(Call<BingDirectionApi> call, Response<BingDirectionApi> response) {
                Log.i("mBingRetrofit","success,"+response.body());
                try {
                    BingDirectionApi bd = response.body();
                    Line l = bd.getResourceSets().get(0).getResources().get(0).getRoutePath().getLine();
                    Log.i("mBingRetrofit line",new Gson().toJson(l));
                    //drawPolylineToMap2(l);
                    rc.onGetDirectionByBing(l);
                    String ss= ""+new Gson().toJson(bd);
                    Log.e("mBingRetrofit s",""+ss);
                }catch (Exception e){
                    Log.e("mBingRetrofit e",e+"");
                }
            }

            @Override
            public void onFailure(Call<BingDirectionApi> call, Throwable t) {
                Log.e("mBingRetrofit f",t.getMessage()+","+t.toString());
            }
        });
    }
}
