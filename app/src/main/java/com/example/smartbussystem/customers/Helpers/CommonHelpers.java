package com.example.smartbussystem.customers.Helpers;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.core.content.ContextCompat;

import com.example.smartbussystem.R;
import com.google.android.gms.maps.model.LatLng;

import static android.content.Context.INPUT_METHOD_SERVICE;

public final class CommonHelpers {
    public static DisplayMetrics deviceScreen = null;

    public static void setDeviceDisplay(Activity activity){
        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        deviceScreen = displaymetrics;
    }


    public static LatLng getUserLatLan(Context mContext) {


        LocationManager lm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        Location myLocation = null;
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
            myLocation = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (myLocation == null) {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_COARSE);
            String provider = lm.getBestProvider(criteria, true);
            myLocation = lm.getLastKnownLocation(provider);
        }


        if(myLocation==null){
            return new LatLng(23.750980, 90.387195);
        }
        return new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
    }

    public static void editTextFocusWithKeyboard(EditText editText,Activity activity){
        Log.i("Keyboard",activity.toString());
        editText.requestFocus();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
    }

    public static void closeKeyBoard(View v, Activity activity){
        Log.i("Keyboard",activity.toString());
        try {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            Log.i("Keyboard","KeyBoard Close success !");
        } catch (Exception e) {
            Log.i("Keyboard","KeyBoard Close Failed !"+e);
        }
    }

    public static String getUrl(LatLng origin, LatLng dest, String directionMode,Context context) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + context.getString(R.string.google_maps_key);
        return url;
    }

    public static boolean validateEditText(EditText ... editTexts){
        boolean flag = true;
        for (EditText editText:editTexts){
            if (TextUtils.isEmpty(editText.getText().toString())){
                editText.setError("Empty");
                flag = false;
            }
        }

        return flag;
    }

}
