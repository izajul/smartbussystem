package com.example.smartbussystem.customers.Helpers;

import com.example.smartbussystem.BingDirectionApi.Line;
import com.example.smartbussystem.customers.modelsAPI.BusStopsModelAPI;

import java.util.List;
import java.util.Map;

public interface RetrofitAPICallBack {
    void onGetDirectionByBing(Line l);
    void onGetBusStoppages(List<BusStopsModelAPI.Data> mDataList);
    void onGetBusById(Map bus);
}
