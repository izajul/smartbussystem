package com.example.smartbussystem.customers.HttpConnector;

import com.example.smartbussystem.BingDirectionApi.BingDirectionApi;
import com.example.smartbussystem.customers.modelsAPI.BusStopsModelAPI;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


public interface ApiKeyContainer {

    @GET
    Call<BingDirectionApi>getRoutesBingMap(@Url String url);


    //@FormUrlEncoded
    @GET("bus_stops?")
    Call<BusStopsModelAPI>getStoppage(@Query("keyword") String keyword);

    @GET("buses/{id}")
    Call<Map>getBusById(@Path("id") int id);

   // Call getDirection(@Url String url);

//    @FormUrlEncoded
//    @POST("route")
    /*Call<MapRequestDirectionModelCustomers>getDirection(
            @Query("outFormat") String outFormat,
            @Query("key") String key,
            @Query("from") String from,
            @Query("to") String to,
            @Query("ambiguities") String ambiguities,
            @Query("routeType") String routeType,
            @Query("doReverseGeocode") String doReverseGeocode,
            @Query("enhancedNarrative") String enhancedNarrative,
            @Query("avoidTimedConditions") String avoidTimedConditions);*/
}
