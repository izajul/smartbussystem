package com.example.smartbussystem.customers.Helpers;

public interface BackPressHelper {
    boolean onBackPress() throws InterruptedException;
}
