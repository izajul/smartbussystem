
package com.example.smartbussystem.customers.modelsAPI;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusStopsModelAPI {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<Data> datas = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Data> getData() {
        return datas;
    }

    public void setData(List<Data> datas) {
        this.datas = datas;
    }
	
	public class Data {

		@SerializedName("id")
		@Expose
		private Integer id;
		@SerializedName("name")
		@Expose
		private String name;
		@SerializedName("is_terminal")
		@Expose
		private Integer isTerminal;
		@SerializedName("location")
		@Expose
		private Location location;

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Integer getIsTerminal() {
			return isTerminal;
		}

		public void setIsTerminal(Integer isTerminal) {
			this.isTerminal = isTerminal;
		}

		public Location getLocation() {
			return location;
		}

		public void setLocation(Location location) {
			this.location = location;
		}

	}

	
	public class Location {
		@SerializedName("lat")
		@Expose
		private String lat;
		@SerializedName("lng")
		@Expose
		private String lng;

		public String getLat() {
			return lat;
		}

		public void setLat(String lat) {
			this.lat = lat;
		}

		public String getLng() {
			return lng;
		}

		public void setLng(String lng) {
			this.lng = lng;
		}
	}

}
