package com.example.smartbussystem.customers.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.smartbussystem.R;
import com.example.smartbussystem.customers.modelsAPI.BusStopsModelAPI;
import com.google.android.libraries.places.api.model.AutocompletePrediction;

import java.util.List;

public class ListViewAdapter_customers extends ArrayAdapter<BusStopsModelAPI.Data> {
    private int resourceLayout;
    private Context mContext;
    private List<BusStopsModelAPI.Data> predictions;

    public ListViewAdapter_customers(Context context, int resource, List<BusStopsModelAPI.Data> items) {
        super(context,resource,items);
        this.resourceLayout = resource;
        this.mContext = context;
        this.predictions = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

       // Log.i("getview","getting View");

        if (v == null) {
            LayoutInflater vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }
        TextView title = v.findViewById(R.id.searched_location_list_view_title);
        TextView description = v.findViewById(R.id.searched_location_list_view_description);

        BusStopsModelAPI.Data prediction = predictions.get(position);

        if (prediction!=null){
            title.setText(prediction.getName());
            description.setText(prediction.getName()+", Dhaka, Bangladesh");
           // Log.i("Place", prediction.getPlaceId());
           // Log.i("Place", prediction.getPrimaryText(null).toString());
           // Log.i("Place", prediction.getSecondaryText(null).toString());
        }
       // Log.i("getview","Prediction"+prediction);
       // Log.i("getview","view"+v);

        return v;
    }
}
