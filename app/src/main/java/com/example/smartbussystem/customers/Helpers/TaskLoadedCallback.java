package com.example.smartbussystem.customers.Helpers;

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
