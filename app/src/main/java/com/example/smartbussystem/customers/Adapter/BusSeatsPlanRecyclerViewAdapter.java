package com.example.smartbussystem.customers.Adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smartbussystem.R;

public class BusSeatsPlanRecyclerViewAdapter extends RecyclerView.Adapter<BusSeatsPlanRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private String mSeats[] = {"A", "B", "C", "D", "E", "F", "G", "H", "I"};
    private SeatClickListener listner;
    private Drawable booked_seat_drawable, not_booked_seat_drawable;

    public BusSeatsPlanRecyclerViewAdapter(Context context, SeatClickListener listner,
                                           Drawable not_booked_seat_drawable,
                                           Drawable booked_seat_drawable) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.listner = listner;
        this.booked_seat_drawable = booked_seat_drawable;
        this.not_booked_seat_drawable = not_booked_seat_drawable;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.bus_seats_plan_4x_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String s = mSeats[position];
        holder.text1.setText(s + "1");
        holder.text2.setText(s + "2");
        holder.text3.setText(s + "3");
        holder.text4.setText(s + "4");

        if (position % 3 == 0) {
            holder.seat1.setImageDrawable(booked_seat_drawable);
            holder.seat1.setTag(booked_seat_drawable);
            holder.seat3.setImageDrawable(booked_seat_drawable);
            holder.seat3.setTag(booked_seat_drawable);

            holder.seat2.setImageDrawable(not_booked_seat_drawable);
            holder.seat2.setTag(not_booked_seat_drawable);
            holder.seat4.setImageDrawable(not_booked_seat_drawable);
            holder.seat4.setTag(not_booked_seat_drawable);
        } else if (position % 3 == 1) {
            holder.seat2.setImageDrawable(booked_seat_drawable);
            holder.seat2.setTag(booked_seat_drawable);
            holder.seat1.setImageDrawable(booked_seat_drawable);
            holder.seat1.setTag(booked_seat_drawable);


            holder.seat3.setImageDrawable(not_booked_seat_drawable);
            holder.seat3.setTag(not_booked_seat_drawable);
            holder.seat4.setImageDrawable(not_booked_seat_drawable);
            holder.seat4.setTag(not_booked_seat_drawable);

        } else {
            holder.seat2.setImageDrawable(not_booked_seat_drawable);
            holder.seat2.setTag(not_booked_seat_drawable);
            holder.seat1.setImageDrawable(not_booked_seat_drawable);
            holder.seat1.setTag(not_booked_seat_drawable);

            holder.seat3.setImageDrawable(not_booked_seat_drawable);
            holder.seat3.setTag(not_booked_seat_drawable);
            holder.seat4.setImageDrawable(not_booked_seat_drawable);
            holder.seat4.setTag(not_booked_seat_drawable);
        }
    }

    @Override
    public int getItemCount() {
        return mSeats.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView text1, text2, text3, text4;
        ImageView seat1, seat2, seat3, seat4;
        FrameLayout fl1, fl2, fl3, fl4;
        LinearLayout ll;

        ViewHolder(View itemView) {
            super(itemView);

            fl1 = itemView.findViewById(R.id.seat_1_FL);
            fl2 = itemView.findViewById(R.id.seat_2_FL);
            fl3 = itemView.findViewById(R.id.seat_3_FL);
            fl4 = itemView.findViewById(R.id.seat_4_FL);

            fl1.setOnClickListener(this);
            fl2.setOnClickListener(this);
            fl3.setOnClickListener(this);
            fl4.setOnClickListener(this);

            text1 = itemView.findViewById(R.id.bus_seat_text_1);
            text2 = itemView.findViewById(R.id.bus_seat_text_2);
            text3 = itemView.findViewById(R.id.bus_seat_text_3);
            text4 = itemView.findViewById(R.id.bus_seat_text_4);

            seat1 = itemView.findViewById(R.id.bus_seat_IMV_1);
            seat2 = itemView.findViewById(R.id.bus_seat_IMV_2);
            seat3 = itemView.findViewById(R.id.bus_seat_IMV_3);
            seat4 = itemView.findViewById(R.id.bus_seat_IMV_4);

            ll = itemView.findViewById(R.id.seat_book_BTN);
        }

        @Override
        public void onClick(View view) {
            TextView tv = null;
            ImageView iv = null;
            for (int i = 0; i < ((FrameLayout) view).getChildCount(); i++) {
                View element = ((FrameLayout) view).getChildAt(i);
                if (element instanceof TextView) tv = (TextView) element;
                if (element instanceof ImageView) iv = (ImageView) element;
            }
            //Log.d("bookbtn",""+ll);
            //ll.setVisibility(View.VISIBLE);

            if (listner != null) listner.onClickSeat(tv, iv, getAdapterPosition());

            //if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public interface SeatClickListener {
        void onClickSeat(TextView tv, ImageView iv, int position);
    }

}
