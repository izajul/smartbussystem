package com.example.smartbussystem.customers.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smartbussystem.BingDirectionApi.Line;
import com.example.smartbussystem.customers.Activites.BookSeatActivity_customers;
import com.example.smartbussystem.customers.Activites.MapsActivity_customers;
import com.example.smartbussystem.customers.Adapter.BusSeatsPlanRecyclerViewAdapter;
import com.example.smartbussystem.customers.Adapter.ListViewAdapter_customers;
import com.example.smartbussystem.customers.Helpers.BackPressHelper;
import com.example.smartbussystem.customers.Helpers.CommonHelpers;
import com.example.smartbussystem.customers.Helpers.RetrofitAPICallBack;
import com.example.smartbussystem.customers.Helpers.RetrofitAPIHelper;
import com.example.smartbussystem.customers.Helpers.TaskLoadedCallback;
import com.example.smartbussystem.R;
import com.example.smartbussystem.customers.modelsAPI.BusStopsModelAPI;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static java.util.Locale.getDefault;

public class MapFragment_customers extends Fragment implements
        GoogleMap.OnMyLocationClickListener, OnMapReadyCallback,
        View.OnClickListener, BackPressHelper, AdapterView.OnItemClickListener,
        TaskLoadedCallback, Animation.AnimationListener, RetrofitAPICallBack, GoogleMap.OnMarkerClickListener {

    //private View mThisViewHolder = null;

    private Activity mActivity;
    private Context mContext;
    private final String PLACE_API_KEY = String.valueOf(R.string.google_place_api_key);

    private LatLng userLatLng ;

    private View mMapView;
    private GoogleMap mMap = null;
    private LinearLayout mSearchDestinationBottomLayout,mSearchDestinationFullLayout,mSearchDestinationBottomLL;
    private ListView mSearchLoactionListView;
    private ImageView myLocationBTN,mBackFromSearchIMV,mCancelUserLocationSearchIMV,mCancelDestinationSearchIMV;
    private EditText mUserLoactionET,mSearchDestinationTopET;

    //private List<AutocompletePrediction> details;

    private List<BusStopsModelAPI.Data> datas ;

    private ListViewAdapter_customers listViewAdapter;

    private Bitmap mMarkerPointerIcon,mMarkerPointerBus ;

    private Polyline currentPolyline;

    private boolean visibilityFlag = false;

    private RetrofitAPIHelper retrofitAPIHelper;

    //google place client
    private PlacesClient placesClient ;
    private AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();
    // Create a RectangularBounds object.
    private RectangularBounds bounds = RectangularBounds.newInstance(
            new LatLng(23.694320, 90.399045),
            new LatLng(24.086833, 90.205272));

    //animation
    private Animation fade_out,slide_down,slide_in_up,slide_out_bottom,fade_in;

    private Marker mMarkerBus ;

    public MapFragment_customers() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i("_map", "map fragment onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);

        //Log.d("TestSaveStage")
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

//        if (mThisViewHolder != null) // return stored view after onResume
//            return mThisViewHolder;

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.customers_fragment_map, container, false);

        mActivity = getActivity();
        mContext = getContext();

        retrofitAPIHelper = new RetrofitAPIHelper();

        BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.location_icon2);
        BitmapDrawable bitmapbus=(BitmapDrawable)getResources().getDrawable(R.drawable.marker_bus);

        mMarkerPointerIcon = Bitmap.createScaledBitmap(bitmapdraw.getBitmap(), 40, 55, false);
        mMarkerPointerBus = Bitmap.createScaledBitmap(bitmapbus.getBitmap(), 29, 90, false);

        //initialized all views
        initializeviews(view);

        //initilized listner
        setupListner();

        //setupTextChangeListner
        setupTextChangeListner(); // setup TextWatcher for mUserLoactionET & mSearchDestinationTopET

        //Google Place Codes.....
        if (!Places.isInitialized()) {
            Places.initialize(mContext, getString(R.string.google_maps_key));
          //  Places.initialize(mContext, "AIzaSyB2Vc_MXrvtSwItzFG3GT0A1mGEJc6IBFc");
            //Places.initialize(mContext,"AIzaSyAbydKKHq8la5fAU-xItc85b3YlKNSPyKc");
            Log.i("KEY",PLACE_API_KEY);
        }

        placesClient = Places.createClient(mContext);

        //Replacing view form drawer with google map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.frg);
        mMapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

        //Initialize Method for some Animation
        initializeAnimation();


       // mThisViewHolder = view; // Saving the view
        return view;
    }

    private void initializeviews(View view){
        mSearchDestinationBottomLayout = view.findViewById(R.id.search_destination_bottom);
        mSearchDestinationBottomLayout.setVisibility(View.VISIBLE);
        mSearchDestinationFullLayout = view.findViewById(R.id.search_destination_full);
        mSearchDestinationFullLayout.setVisibility(View.GONE);
        mSearchDestinationBottomLL = view.findViewById(R.id.search_destination_bottom_LL);
        mSearchLoactionListView = view.findViewById(R.id.searched_location_list_view);
        mBackFromSearchIMV = view.findViewById(R.id.back_from_search_full_IV);
        mCancelUserLocationSearchIMV =  view.findViewById(R.id.cancel_user_location_IM);
        mCancelDestinationSearchIMV = view.findViewById(R.id.cancel_search_IV);
        mUserLoactionET = view.findViewById(R.id.user_Location_ET);
        mSearchDestinationTopET = view.findViewById(R.id.search_destination_top_ET);
        myLocationBTN = view.findViewById(R.id.my_location_btn);

    }

    private void setupListner(){
        // LinearLayout View Initializing with setVisibility
        mSearchDestinationBottomLL.setOnClickListener(this);

        //Location search ListView Initializing
        mSearchLoactionListView.setOnItemClickListener(this);

        //ImageView Initializing OnClick Method
        mBackFromSearchIMV.setOnClickListener(this);

        mCancelUserLocationSearchIMV.setOnClickListener(this);

        //custom location onclick method
        myLocationBTN.setOnClickListener(this);
    }

    @SuppressLint("ResourceType")
    private void initializeAnimation(){
        fade_out = AnimationUtils.loadAnimation(mContext,R.animator.fade_out);
        fade_out.setAnimationListener(this);

        slide_down = AnimationUtils.loadAnimation(mContext,R.animator.slide_down);
        slide_down.setAnimationListener(this);

        slide_in_up = AnimationUtils.loadAnimation(mContext,R.animator.animation_slie_in_up);
        slide_in_up.setAnimationListener(this);

        slide_out_bottom = AnimationUtils.loadAnimation(mContext,R.animator.animation_slide_out_bottom);
        slide_out_bottom.setAnimationListener(this);

        fade_in = AnimationUtils.loadAnimation(mContext,R.animator.fade_in);
        fade_in.setAnimationListener(this);
    }

    private void visibaleSearchPlace(){
        mSearchDestinationBottomLayout.startAnimation(fade_out); //set animation
        MapsActivity_customers.navigate_drawer_btn.setVisibility(View.GONE);
        visibilityFlag = true;
    }

    private void invisibaleSearchPlace(){
        CommonHelpers.closeKeyBoard(mSearchDestinationTopET,mActivity);
        slide_out_bottom.setStartOffset(100);
        mSearchDestinationFullLayout.startAnimation(slide_out_bottom);
        //requesting focusing and set Keyboard in EditText
        visibilityFlag = false;
    }

    private void setupTextChangeListner(){
        mSearchDestinationTopET.addTextChangedListener(new TextWatcher() {
            //Text Change Listeners for EditText
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.i("TextChangeListener", "beforeTextChange");
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.i("TextChangeListener", "OnTextChange");
                setAtutoCompleateLocation(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (listViewAdapter!=null){
                    listViewAdapter.notifyDataSetChanged();
                }
                Log.i("TextChangeListener", "AfterTextChange");
            }

        });
        mUserLoactionET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                setAtutoCompleateLocation(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (listViewAdapter!=null){
                    listViewAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    private void setAtutoCompleateLocation(CharSequence charSequence){

        //Call Api for get Bus Stoppages
        //The Data Will be Observe by a Override method -> onGetBusStoppages() as call back;
        retrofitAPIHelper.getBusStoppage(charSequence,this);

         // Auto Complete Code By google
      /*  FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                // Call either setLocationBias() OR setLocationRestriction().
                .setLocationBias(bounds)
                //.setLocationRestriction(bounds)
                .setCountry("bd")//Bangladesh
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(token)
                .setQuery(charSequence.toString())
                .build();

        placesClient.findAutocompletePredictions(request).addOnSuccessListener(
                new OnSuccessListener<FindAutocompletePredictionsResponse>() {
                    @Override
                    public void onSuccess(FindAutocompletePredictionsResponse response) {

                        Log.i("autoComplete", "success");
                        //ArrayList<AutocompletePrediction> predictions = new ArrayList<>();
                for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                   // predictions.add(prediction);
                    Log.i("Place", prediction.getPlaceId());
                    Log.i("Place", prediction.getPrimaryText(null).toString());
                    Log.i("Place", prediction.getSecondaryText(null).toString());

                }
                        details = response.getAutocompletePredictions();
                        listViewAdapter = new ListViewAdapter_customers(mContext,R.layout.customers_searched_loaction_list_style,details);
                        mSearchLoactionListView.setAdapter(listViewAdapter);
                        Log.i("autoComplete", listViewAdapter.toString());

                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    Log.e("autoComplete", "Place not found: " + apiException.getStatusCode());
                    Log.e("autoComplete", "Place not found: " + exception);
                }
            }
        });*/

    }

    @Override
    public void onMapReady(GoogleMap mMap) {
      //  Log.i("_map", "Inside On Map ready");
      //  Log.i("_map", "Inside On Map ready" + mMap.toString());
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.mMap = mMap;
        mMap.setOnMarkerClickListener(this);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        if (ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED)
            mMap.setMyLocationEnabled(true);
        mMap.setOnMyLocationClickListener(this);

        View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        if (locationButton!=null){
            locationButton.setVisibility(View.GONE);
        }
        userLatLng = CommonHelpers.getUserLatLan(mContext);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 14), 1, null);

/*
        //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10000, null);

        mMap.clear(); //clear old markers

                CameraPosition googlePlex = CameraPosition.builder()
                        .target(new LatLng(23.750980, 90.387195))
                        .zoom(10)
                        .bearing(0)
                        .tilt(45)
                        .build();

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10000, null);

                //add marker three times
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(23.750980, 90.387195))
                        .title("Spider Man"));

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(23.749943, 90.392957))
                        .title("Iron Man")
                        .snippet("His Talent : Plenty of money"));

                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(23.758005, 90.390243))
                        .title("Captain America")); */
    }

    @Override
    public void onAttach(Context context) {
        Log.i("_map","map fragment attach");
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        Log.i("_map","map Fragment detach");
        super.onDetach();
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
    }

    @Override
    public void onClick(View view) {
        if (view == myLocationBTN){
            if (mMap!=null){
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(CommonHelpers.getUserLatLan(mContext),
                        16), 1000, null);
            }
        }
        if (view == mSearchDestinationBottomLL){
            visibaleSearchPlace();
            LatLng ll = CommonHelpers.getUserLatLan(mContext);
            mUserLoactionET.setText(getAddressByLatLan(ll));
        }
        if (view == mBackFromSearchIMV){
            this.onBackPress();
        }
        if (view == mCancelUserLocationSearchIMV){
           mUserLoactionET.setText("");
           mUserLoactionET.requestFocus();
        }
    }

    private String getAddressByLatLan(LatLng ll){
        Geocoder gcd = new Geocoder(mContext, getDefault());
        List<Address> addresses;
        String addressName ="";
        try {
            addresses = gcd.getFromLocation(ll.latitude,
                    ll.longitude, 1);
            if (addresses.size() > 0){
                String s = "Address Line: "
                        + addresses.get(0).getAddressLine(0) + "\n"
                        + addresses.get(0).getFeatureName() + "\n"
                        + "Locality: "
                        + addresses.get(0).getLocality() + "\n"
                        + addresses.get(0).getPremises() + "\n"
                        + "Admin Area: "
                        + addresses.get(0).getAdminArea() + "\n"
                        + "Country code: "
                        + addresses.get(0).getCountryCode() + "\n"
                        + "Country name: "
                        + addresses.get(0).getCountryName() + "\n"
                        + "Phone: " + addresses.get(0).getPhone()
                        + "\n" + "Postbox: "
                        + addresses.get(0).getPostalCode() + "\n"
                        + "SubLocality: "
                        + addresses.get(0).getSubLocality() + "\n"
                        + "SubAdminArea: "
                        + addresses.get(0).getSubAdminArea() + "\n"
                        + "SubThoroughfare: "
                        + addresses.get(0).getSubThoroughfare()
                        + "\n" + "Thoroughfare: "
                        + addresses.get(0).getThoroughfare() + "\n"
                        + "URL: " + addresses.get(0).getUrl();

                Log.i("geocoder",s);
                addressName = addresses.get(0).getFeatureName() +", "+
                        addresses.get(0).getSubLocality() +", "+
                        addresses.get(0).getLocality() + " "+
                        addresses.get(0).getPostalCode() + ", "+
                        addresses.get(0).getCountryName();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        //Log.i("geocoder",addressName);
        return addressName;
    }

    @Override
    public boolean onBackPress(){
        if (visibilityFlag){
            invisibaleSearchPlace();
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        if (datas!=null){
        //    Data prediction = datas.get(i);
        //    getLatLanById(prediction.getPlaceId());
            //Log.i("onItemClick","place:"+pl);

            BusStopsModelAPI.Data data = datas.get(i);
            BusStopsModelAPI.Location location = data.getLocation();
            LatLng ll = new LatLng(Double.valueOf(location.getLat()),Double.valueOf(location.getLng()));
            String address = getAddressByLatLan(ll);

            if(mUserLoactionET.isFocused()){
                    userLatLng = ll;
                    mUserLoactionET.setText(address);
                    mSearchDestinationTopET.requestFocus();
            }else {
                setLocationBound(ll,address);
                retrofitAPIHelper.getDirectionByBing(ll,userLatLng,MapFragment_customers.this);
            }
        }

    }

    //todo:: useless method till now..
    private void getLatLanById(final String placeId){
        // Specify the fields to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG,Place.Field.TYPES);
// Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.newInstance(placeId, placeFields);

        placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
            public void onSuccess(FetchPlaceResponse response) {
                Place pl = response.getPlace();
                if(mUserLoactionET.isFocused()){
                   // Log.i("onItemClick","focused UserET");
                    if (pl!=null){
                        userLatLng = pl.getLatLng();
                        mUserLoactionET.setText(getAddressByLatLan(pl.getLatLng()));
                        mSearchDestinationTopET.requestFocus();
                    }

                }else {
                   // Log.i("onItemClick","Not focused UserET");
                    if (pl!=null){
                        //setLocationBound(pl);
                       //Todo:: getDirection(userLatLng,pl.getLatLng());
                        retrofitAPIHelper.getDirectionByBing(userLatLng,pl.getLatLng(),MapFragment_customers.this);
                    }
                }
               // Log.i("placeClient", "Place found: " + pl.getName());
              // Log.i("placeClient", "Place found: " + pl.getLatLng().toString());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                if (exception instanceof ApiException) {
                    Log.e("placeClient", "Place not found: " + exception.getMessage());
                }
            }
        });
       // Log.e("placeClient", "place:"+place[0]);
    }

    private void setLocationBound(LatLng place,String address){
            invisibaleSearchPlace();

            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(userLatLng).include(place);
            mMap.clear();
//TODO: here belwo FetchURL will be active when Google Distance
//                    new FetchURL(MapFragment_customers.this).execute(CommonHelpers.getUrl(userLatLng,
//                            place.getLatLng(), "driving", mContext));
            mMap.addMarker(new MarkerOptions()
                    .position(place)
                    .title(address)
                    .icon(BitmapDescriptorFactory.fromBitmap(mMarkerPointerIcon))).showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 5), 1, null);
    }

    private void drawPolylineToMap2(Line l) {
        PolylineOptions pi = new PolylineOptions();

        List<List<Double>> lines = l.getCoordinates();

        for (List<Double> line:lines){
            try {
                String point = String.valueOf(line).replace("[", "").replace("]", "");
                String[] points = point.split(",");
                Log.i("mBingRetrofit lat-lang", point+"");
                LatLng ll = new LatLng(Double.valueOf(points[0]), Double.valueOf(points[1]));
                Log.i("mBingRetrofit lat-lang", ll.toString() + " bing ");
                pi.add(ll);
            }catch (Exception e){
                Log.e("mBingRetrofit parse","Point Parse Faild:"+e.toString());
            }

        }
        float rotation = 0;
        try {
            String point1 = String.valueOf(lines.get(2)).replace("[", "").replace("]", "");
            String point2 = String.valueOf(lines.get(3)).replace("[", "").replace("]", "");
            String[] points1 = point1.split(",");
            String[] points2 = point2.split(",");

            LatLng pre = new LatLng(Double.valueOf(points1[0]), Double.valueOf(points1[1]));
            LatLng nxt = new LatLng(Double.valueOf(points2[0]), Double.valueOf(points2[1]));

            rotation = getBeaingRotation(pre,nxt);
            mMap.addPolyline(pi);
            mMarkerBus = mMap.addMarker(new MarkerOptions()
                    .rotation(rotation)
                    .position(pre).icon(BitmapDescriptorFactory.fromBitmap(mMarkerPointerBus)).flat(true));
            Log.i("mBingRetrofit ","rotation "+rotation);
            Log.i("mBingRetrofit ","bus  "+mMarkerBus.getPosition());
        }catch (Exception e){

        }
    }

    private float getBeaingRotation(LatLng pre,LatLng nxt){

        Location preLoc = new Location(LocationManager.GPS_PROVIDER);
        preLoc.setLatitude(pre.latitude);
        preLoc.setLongitude(pre.longitude);

        Location nxtLoc = new Location(LocationManager.GPS_PROVIDER);
        nxtLoc.setLatitude(nxt.latitude);
        nxtLoc.setLongitude(nxt.longitude);

        return preLoc.bearingTo(nxtLoc);
    }

    @Override
    public void onTaskDone(Object... values) {
        Log.e("placeClient", "callTaskDone inside: ");
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == fade_out){
            mSearchDestinationBottomLayout.setVisibility(View.GONE);
            mSearchDestinationFullLayout.setVisibility(View.VISIBLE);
            mSearchDestinationFullLayout.startAnimation(slide_in_up);
        }
        if (animation == fade_in){

        }
        if (animation == slide_in_up){
            //requesting focusing and set Keyboard in EditText
            CommonHelpers.editTextFocusWithKeyboard(mSearchDestinationTopET,mActivity);
        }
        if (animation == slide_out_bottom){
            mSearchDestinationFullLayout.setVisibility(View.GONE);
            mSearchDestinationBottomLayout.setVisibility(View.VISIBLE);
            mSearchDestinationBottomLayout.startAnimation(fade_in);
            MapsActivity_customers.navigate_drawer_btn.setVisibility(View.VISIBLE);
        }
        if (animation == slide_down){

        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onGetDirectionByBing(Line l) {
       // Log.i("Callback","onGetDirectionByBing"+l.toString());
        if (l!=null){
            drawPolylineToMap2(l);
        }
    }

    @Override
    public void onGetBusStoppages(List<BusStopsModelAPI.Data> mDataList) {
        if (mDataList!=null){
            this.datas = mDataList;
            listViewAdapter = new ListViewAdapter_customers(mContext,R.layout.customers_searched_loaction_list_style,datas);
            mSearchLoactionListView.setAdapter(listViewAdapter);
        }
    }

    @Override
    public void onGetBusById(Map bus) {
        Log.d("Busss","CallBack from getBusById");
        Log.d("Busss","data"+bus.get("data"));
        Map m = (Map) bus.get("data");
        Log.d("Busss","seats"+m.get("seats"));
        Log.d("Busss","columns"+m.get("seat_columns"));
        Log.d("Busss","seat_planning"+m.get("seat_planning"));

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
       // Log.d("Busss","inside method");
        //Log.d("Busss","inside method"+marker+","+mMarkerBus);
        if ((marker.equals(mMarkerBus)) && mMarkerBus!=null){
           retrofitAPIHelper.getBusById(2,this); //this result will shon in onGetBusById() method
            Log.d("Busss","markerClicked"+marker.getPosition());

            startActivity(new Intent(mContext,BookSeatActivity_customers.class));

            //show_bus_sets();
        }
        return false;
    }



    {
        /*
    private void show_bus_sets() {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.bus_seats_plan_layout);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        dialog.getWindow().setAttributes(lp);

        String[] getway = { "bKash", "Rocket", "Ucash", "Credit Card", "Debit Card"};

        final RelativeLayout payment = dialog.findViewById(R.id.payment_option);
        final RelativeLayout seatplan = dialog.findViewById(R.id.seat_list);

        final Spinner spinner = dialog.findViewById(R.id.spinner1);
        ArrayAdapter<String>adapter = new ArrayAdapter<>(mContext,R.layout.support_simple_spinner_dropdown_item,getway);
        spinner.setAdapter(adapter);


        final RecyclerView recycler_spinner = dialog.findViewById(R.id.bus_seat_plan_RCV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recycler_spinner.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, true));
        recycler_spinner.setLayoutManager(layoutManager);
        seatsPlanRecyclerViewAdapter = new BusSeatsPlanRecyclerViewAdapter(mContext,
                new BusSeatsPlanRecyclerViewAdapter.SeatClickListener() {
            @Override
            public void onClickSeat(TextView tv,ImageView iv, int position) {
              *//*  Log.d("busseat","wow:"+tv.getText()+",wow:"+iv.getId()+",draw: "+iv.getDrawable());
                Log.d("busseat","drawable:"+booked_seat_drawable);
                Log.d("busseat","drawable:"+not_booked_seat_drawable);
                Log.d("busseat","drawable:"+selected_seat_drawable);
                if (iv.getDrawable().equals(booked_seat_drawable)){
                    Log.d("busseat","booked_seat_drawable");
                }else if (iv.getDrawable().equals(not_booked_seat_drawable)){
                    Log.d("busseat","not_booked_seat_drawable");
                    iv.setImageDrawable(selected_seat_drawable);
                }else if (iv.getDrawable().equals(selected_seat_drawable)) {
                    Log.d("busseat", "selected_seat_drawable");
                    iv.setImageDrawable(selected_seat_drawable);
                }*//*
              Log.d("IV-Tag:",""+selected_seat_drawable);
              Log.d("IV-Tag:",""+not_booked_seat_drawable);
              Log.d("IV-Tag:",""+booked_seat_drawable);
              Log.d("IV-Tag:",""+iv.getTag());
              if(iv.getTag()!=null) {
                if(iv.getTag()==not_booked_seat_drawable){
                    iv.setImageDrawable(selected_seat_drawable);
                    iv.setTag(selected_seat_drawable);
                    mSeatBookingCounter++;
                }else if (iv.getTag()==selected_seat_drawable){
                    iv.setImageDrawable(not_booked_seat_drawable);
                    iv.setTag(not_booked_seat_drawable);
                    mSeatBookingCounter--;
                }else{
                    Toast.makeText(mActivity, "Seat Already Booked", Toast.LENGTH_SHORT).show();
                }
              }

            }
        },not_booked_seat_drawable,booked_seat_drawable,selected_seat_drawable);
        recycler_spinner.setAdapter(seatsPlanRecyclerViewAdapter);
        dialog.show();

        ImageView back =  dialog.findViewById(R.id.back_from_seat_plan_ImgBTN);
        Button seat_book_BTN =  dialog.findViewById(R.id.seat_book_BTN);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (payment.getVisibility()==View.VISIBLE) {

                    seatplan.setVisibility(View.VISIBLE);
                    payment.setVisibility(View.GONE);
                }else
                    dialog.dismiss();
            }
        });
        seat_book_BTN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("bookbtn:","clicking Book BTN");
                if(mSeatBookingCounter>0){


                    seatplan.setVisibility(View.GONE);
                    payment.setVisibility(View.VISIBLE);

                }else{
                    Toast.makeText(mActivity, "Select A Seat First!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    */

    }

}
