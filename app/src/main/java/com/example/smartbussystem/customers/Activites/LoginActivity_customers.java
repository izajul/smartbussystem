package com.example.smartbussystem.customers.Activites;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.smartbussystem.customers.Helpers.CommonHelpers;
import com.example.smartbussystem.R;

import static com.example.smartbussystem.customers.Helpers.CommonHelpers.validateEditText;

public class LoginActivity_customers extends AppCompatActivity
        implements View.OnClickListener, Animation.AnimationListener, KeyEvent.Callback{

    LinearLayout before_login_click,after_login_click,after_login_code_send;
    RelativeLayout login_form_hole_view_RL;
    ImageButton loginBTN,back_from_login_phone_code;
    EditText login_phone_ET,code_1,code_2,code_3,code_4;
    TextView login_phone_TV,login_code_top_TV,code_wirning_TV,continue_for_sms_notice;
    boolean isPhoneWanted = false, isCodeWanted = false;

    private Animation slideUp,slideOut,slideBottom,slideBottomOut,
            slideFromLeft,slideOutToLeft,slideFromRight,slide_out_rgiht,slide_to_right,slideDown;

    private SharedPreferences sharedPreferences ;
    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customers_activity_login);

        sharedPreferences = getSharedPreferences("login",MODE_PRIVATE);

        Log.e("login_form",sharedPreferences.getInt("login",0)+"");

        if (sharedPreferences.getInt("login",0)==1){
            startActivity(new Intent(this,MapsActivity_customers.class));
            finish();
        }

        //TODO:: This changing Activity is just for skipping login Activity..
        startActivity(new Intent(this, MapsActivity_customers.class));

        //Initializing All View
        initViews();

        //initializing all Animation
        animationInitializing();

        CommonHelpers.setDeviceDisplay(this);

        Log.i("loginActivity","width:"+CommonHelpers.deviceScreen.widthPixels+" Height:"+CommonHelpers.deviceScreen.heightPixels);

        setupTextWatcher();


    }

    private void initViews() {
        loginBTN = findViewById(R.id.login_button_bt);
        loginBTN.setOnClickListener(this);

        login_phone_TV = findViewById(R.id.login_phone_TV);
        login_phone_TV.setOnClickListener(this);

        login_phone_ET = findViewById(R.id.login_phone_ET);
        login_phone_ET.setOnClickListener(this);

        before_login_click = findViewById(R.id.before_login_click);
        before_login_click.setOnClickListener(this);

        after_login_click = findViewById(R.id.after_login_click);
        after_login_click.setOnClickListener(this);

        after_login_code_send = findViewById(R.id.after_login_code_send);
        after_login_code_send.setOnClickListener(this);

        login_form_hole_view_RL=findViewById(R.id.login_form_hole_view_RL);
        login_form_hole_view_RL.setOnClickListener(this);

        back_from_login_phone_code = findViewById(R.id.back_from_login_phone_code);
        back_from_login_phone_code.setOnClickListener(this);

        login_code_top_TV = findViewById(R.id.login_code_top_TV);

        code_1 = findViewById(R.id.code_1);
        code_2 = findViewById(R.id.code_2);
        code_3 = findViewById(R.id.code_3);
        code_4 = findViewById(R.id.code_4);

        code_wirning_TV = findViewById(R.id.code_wirning_TV);
        continue_for_sms_notice = findViewById(R.id.continue_for_sms_notice);

    }

    @SuppressLint("ResourceType")
    private void animationInitializing() {
        slideUp = AnimationUtils.loadAnimation(this, R.animator.animation_slie_in_up);
        slideUp.setAnimationListener(this);
        slideOut = AnimationUtils.loadAnimation(this, R.animator.animation_slide_out_top);
        slideOut.setAnimationListener(this);
        slideBottom = AnimationUtils.loadAnimation(this, R.animator.animation_slide_bottom);
        slideBottom.setAnimationListener(this);
        slideBottomOut = AnimationUtils.loadAnimation(this,R.animator.animation_slide_out_bottom);
        slideBottomOut.setAnimationListener(this);
        slideFromLeft = AnimationUtils.loadAnimation(this,R.animator.animation_slie_from_left);
        slideFromLeft.setAnimationListener(this);
        slideOutToLeft = AnimationUtils.loadAnimation(this,R.animator.animation_slide_out_to_left);
        slideOutToLeft.setAnimationListener(this);
        slideFromRight = AnimationUtils.loadAnimation(this,R.animator.animation_slide_from_right);
        slideFromRight.setAnimationListener(this);
        slide_to_right = AnimationUtils.loadAnimation(this,R.animator.animation_slide_to_right);
        slide_to_right.setAnimationListener(this);
        slide_out_rgiht = AnimationUtils.loadAnimation(this,R.animator.animation_slide_out_rgiht);
        slide_out_rgiht.setAnimationListener(this);
        slideDown = AnimationUtils.loadAnimation(this,R.animator.slide_down);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_button_bt:{
                if (isPhoneWanted){
                    if (TextUtils.isEmpty(login_phone_ET.getText().toString())) {
                        login_phone_ET.setError("Must Enter a Phone Number");
                        Log.i("login_form","indside isPhoneWanted empty");
                    }
                    else{
                        Log.i("login_form","indside isPhoneWanted not empty");
                        after_login_click.startAnimation(slideOutToLeft);
                        after_login_code_send.startAnimation(slideFromRight);
                        after_login_code_send.setVisibility(View.VISIBLE);
                        isPhoneWanted = false;
                        isCodeWanted = true;
                        String s ="Enter The 4-digit code which sent at +880"+login_phone_ET.getText().toString();
                        login_code_top_TV.setText(s);
                        CommonHelpers.editTextFocusWithKeyboard(code_1,this);
                    }
                }else if (isCodeWanted){
                   // Log.i("login_form","indside isCodeWanted " +validateEditText(code_1,code_2,code_3,code_4));
                    if (validateEditText(code_1,code_2,code_3,code_4)){
                        Log.i("login_form","indside isCodeWanted not empty");
                        String code = code_1.getText().toString()
                                +code_2.getText().toString()
                                +code_3.getText().toString()
                                +code_4.getText().toString();
                        Log.i("login_form","indside isPhoneWanted code is"+Integer.valueOf(code));
                        if (Integer.valueOf(code)==1234){
                            isCodeWanted = false;
                            Log.i("login_form","indside isPhoneWanted code is"+Integer.valueOf(code));

                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putInt("login",1);
                            editor.apply();
                            Log.i("aaaaaaaa","execution ");
                            startActivity(new Intent(this, MapsActivity_customers.class));
                            this.finish();
                            Log.i("aaaaaaaa","execution after finish");
                            Log.i("aaaaaaaa","execution after finish login:"+sharedPreferences.getInt("login",0));
                        }
                    }
                }
            }break;
            case R.id.login_phone_TV:{
                isPhoneWanted = true;
                before_login_click.startAnimation(slideOut);
                login_form_hole_view_RL.setVisibility(View.VISIBLE);
                login_form_hole_view_RL.startAnimation(slideUp);
                Log.i("login_form","indside login_phone_TV ");
            }break;
            case R.id.login_phone_ET:
            case R.id.before_login_click:
            case R.id.after_login_click:
            case R.id.after_login_code_send:
            case R.id.login_form_hole_view_RL: {

            }break;
            case R.id.back_from_login_phone_code:{
                this.onBackPressed();
            }break;

        }
    }

    @Override
    public void onBackPressed(){
        Log.i("login_form","form backpress isCode="+isCodeWanted+" isPhone="+isPhoneWanted);
        if (isPhoneWanted){
            Log.i("login_form","form backpress inside isPhoneWanted");
            isPhoneWanted=false;
            isCodeWanted=false;
            login_form_hole_view_RL.startAnimation(slideBottomOut);
            before_login_click.setVisibility(View.VISIBLE);
            before_login_click.startAnimation(slideBottom);
            loginBTN.setVisibility(View.GONE);
            CommonHelpers.closeKeyBoard(after_login_click,this);
        }else if (isCodeWanted){
            Log.i("login_form","form backpress inside isCodeWanted");
            isCodeWanted=false;
            isPhoneWanted=true;
            code_wirning_TV.setVisibility(View.GONE);
            after_login_click.setVisibility(View.VISIBLE);
            after_login_click.startAnimation(slide_to_right);
            after_login_code_send.startAnimation(slide_out_rgiht);


        }else{
            Log.i("login_form","form backpress inside super");
            super.onBackPressed();
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == slideOut){
            if (isPhoneWanted) {
                before_login_click.clearAnimation();
                before_login_click.setVisibility(View.GONE);
            }else{

            }
            Log.i("login_form","indside Animation End SlideOut ");
        }
        if (animation==slideUp){
            loginBTN.setVisibility(View.VISIBLE);
            CommonHelpers.editTextFocusWithKeyboard(login_phone_ET,this);
            Log.i("login_form","indside Animation End SlideUP ");
            login_form_hole_view_RL.clearAnimation();
        }
        if (animation==slideBottomOut){
            Log.i("login_form","indside Animation End slideBottomOut ");
            login_form_hole_view_RL.setVisibility(View.GONE);
            login_form_hole_view_RL.clearAnimation();
        }
        if (animation==slideBottom){
            before_login_click.clearAnimation();
        }
        if (animation == slideFromLeft){

        }
        if (animation == slideOutToLeft){
            after_login_click.setVisibility(View.GONE);
            after_login_click.clearAnimation();
        }
        if (animation == slideFromRight){
            after_login_code_send.clearAnimation();
        }
        if (animation == slide_to_right){
            after_login_click.clearAnimation();
            CommonHelpers.editTextFocusWithKeyboard(login_phone_ET,this);
        }
        if (animation == slide_out_rgiht){
            after_login_code_send.setVisibility(View.GONE);
            after_login_code_send.clearAnimation();
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }


    private void setupTextWatcher() {
        code_1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==1) code_2.requestFocus();
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code_2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==1) code_3.requestFocus();
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code_3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==1) code_4.requestFocus();
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        code_4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length()==1) {
                    String code = code_1.getText().toString()
                            +code_2.getText().toString()
                            +code_3.getText().toString()
                            +code_4.getText().toString();
                    Log.i("login_form","indside isPhoneWanted code is"+code);
                    if (Integer.valueOf(code)==1234){
                        isCodeWanted = false;
                        Log.i("login_form","indside isPhoneWanted code is"+Integer.valueOf(code));
                        startActivity(new Intent(LoginActivity_customers.this, MapsActivity_customers.class));
                    }else{
                        code_wirning_TV.setVisibility(View.VISIBLE);
                        code_wirning_TV.startAnimation(slideDown);
                    }
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
       // Log.i("keyboard_Key","on KeyUp ");
        switch (keyCode) {
            case KeyEvent.KEYCODE_DEL:
                if (isCodeWanted){
                    if (code_1.isFocused()){
                        code_1.setText("");
                    }else if(code_2.isFocused()){
                        code_2.setText("");
                        code_1.requestFocus();
                    }else if(code_3.isFocused()){
                        code_3.setText("");
                        code_2.requestFocus();
                    }else if(code_4.isFocused()){
                        code_4.setText("");
                        code_3.requestFocus();
                    }
                }
                Log.i("keyboard_Key","del key Pressed");
                return true;
            default:
                return super.onKeyUp(keyCode, event);
        }
    }
}
