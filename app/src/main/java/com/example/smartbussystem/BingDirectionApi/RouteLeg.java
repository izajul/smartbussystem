
package com.example.smartbussystem.BingDirectionApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RouteLeg {

    @SerializedName("actualEnd")
    @Expose
    private ActualEnd actualEnd;
    @SerializedName("actualStart")
    @Expose
    private ActualStart actualStart;
    @SerializedName("alternateVias")
    @Expose
    private List<Object> alternateVias = null;
    @SerializedName("cost")
    @Expose
    private Double cost;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("itineraryItems")
    @Expose
    private List<ItineraryItem> itineraryItems = null;
    @SerializedName("routeRegion")
    @Expose
    private String routeRegion;
    @SerializedName("routeSubLegs")
    @Expose
    private List<RouteSubLeg> routeSubLegs = null;
    @SerializedName("travelDistance")
    @Expose
    private Double travelDistance;
    @SerializedName("travelDuration")
    @Expose
    private Double travelDuration;

    public ActualEnd getActualEnd() {
        return actualEnd;
    }

    public void setActualEnd(ActualEnd actualEnd) {
        this.actualEnd = actualEnd;
    }

    public ActualStart getActualStart() {
        return actualStart;
    }

    public void setActualStart(ActualStart actualStart) {
        this.actualStart = actualStart;
    }

    public List<Object> getAlternateVias() {
        return alternateVias;
    }

    public void setAlternateVias(List<Object> alternateVias) {
        this.alternateVias = alternateVias;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<ItineraryItem> getItineraryItems() {
        return itineraryItems;
    }

    public void setItineraryItems(List<ItineraryItem> itineraryItems) {
        this.itineraryItems = itineraryItems;
    }

    public String getRouteRegion() {
        return routeRegion;
    }

    public void setRouteRegion(String routeRegion) {
        this.routeRegion = routeRegion;
    }

    public List<RouteSubLeg> getRouteSubLegs() {
        return routeSubLegs;
    }

    public void setRouteSubLegs(List<RouteSubLeg> routeSubLegs) {
        this.routeSubLegs = routeSubLegs;
    }

    public Double getTravelDistance() {
        return travelDistance;
    }

    public void setTravelDistance(Double travelDistance) {
        this.travelDistance = travelDistance;
    }

    public Double getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(Double travelDuration) {
        this.travelDuration = travelDuration;
    }

}
