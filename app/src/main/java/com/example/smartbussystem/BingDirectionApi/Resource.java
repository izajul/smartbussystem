
package com.example.smartbussystem.BingDirectionApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Resource {

    @SerializedName("__type")
    @Expose
    private String type;
    @SerializedName("bbox")
    @Expose
    private List<Double> bbox = null;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("distanceUnit")
    @Expose
    private String distanceUnit;
    @SerializedName("durationUnit")
    @Expose
    private String durationUnit;
    @SerializedName("routeLegs")
    @Expose
    private List<RouteLeg> routeLegs = null;
    @SerializedName("routePath")
    @Expose
    private RoutePath routePath;
    @SerializedName("trafficCongestion")
    @Expose
    private String trafficCongestion;
    @SerializedName("trafficDataUsed")
    @Expose
    private String trafficDataUsed;
    @SerializedName("travelDistance")
    @Expose
    private Double travelDistance;
    @SerializedName("travelDuration")
    @Expose
    private Double travelDuration;
    @SerializedName("travelDurationTraffic")
    @Expose
    private Double travelDurationTraffic;
    @SerializedName("travelMode")
    @Expose
    private String travelMode;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Double> getBbox() {
        return bbox;
    }

    public void setBbox(List<Double> bbox) {
        this.bbox = bbox;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistanceUnit() {
        return distanceUnit;
    }

    public void setDistanceUnit(String distanceUnit) {
        this.distanceUnit = distanceUnit;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public List<RouteLeg> getRouteLegs() {
        return routeLegs;
    }

    public void setRouteLegs(List<RouteLeg> routeLegs) {
        this.routeLegs = routeLegs;
    }

    public RoutePath getRoutePath() {
        return routePath;
    }

    public void setRoutePath(RoutePath routePath) {
        this.routePath = routePath;
    }

    public String getTrafficCongestion() {
        return trafficCongestion;
    }

    public void setTrafficCongestion(String trafficCongestion) {
        this.trafficCongestion = trafficCongestion;
    }

    public String getTrafficDataUsed() {
        return trafficDataUsed;
    }

    public void setTrafficDataUsed(String trafficDataUsed) {
        this.trafficDataUsed = trafficDataUsed;
    }

    public Double getTravelDistance() {
        return travelDistance;
    }

    public void setTravelDistance(Double travelDistance) {
        this.travelDistance = travelDistance;
    }

    public Double getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(Double travelDuration) {
        this.travelDuration = travelDuration;
    }

    public Double getTravelDurationTraffic() {
        return travelDurationTraffic;
    }

    public void setTravelDurationTraffic(Double travelDurationTraffic) {
        this.travelDurationTraffic = travelDurationTraffic;
    }

    public String getTravelMode() {
        return travelMode;
    }

    public void setTravelMode(String travelMode) {
        this.travelMode = travelMode;
    }

}
