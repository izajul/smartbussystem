
package com.example.smartbussystem.BingDirectionApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResourceSet {

    @SerializedName("estimatedTotal")
    @Expose
    private Double estimatedTotal;
    @SerializedName("resources")
    @Expose
    private List<Resource> resources = null;

    public Double getEstimatedTotal() {
        return estimatedTotal;
    }

    public void setEstimatedTotal(Double estimatedTotal) {
        this.estimatedTotal = estimatedTotal;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

}
