
package com.example.smartbussystem.BingDirectionApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hint {

    @SerializedName("hintType")
    @Expose
    private String hintType;
    @SerializedName("text")
    @Expose
    private String text;

    public String getHintType() {
        return hintType;
    }

    public void setHintType(String hintType) {
        this.hintType = hintType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
