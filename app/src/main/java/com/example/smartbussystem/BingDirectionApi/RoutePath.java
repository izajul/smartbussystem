
package com.example.smartbussystem.BingDirectionApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoutePath {

    @SerializedName("generalizations")
    @Expose
    private List<Object> generalizations = null;
    @SerializedName("line")
    @Expose
    private Line line;

    public List<Object> getGeneralizations() {
        return generalizations;
    }

    public void setGeneralizations(List<Object> generalizations) {
        this.generalizations = generalizations;
    }

    public Line getLine() {
        return line;
    }

    public void setLine(Line line) {
        this.line = line;
    }

}
