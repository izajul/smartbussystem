
package com.example.smartbussystem.BingDirectionApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Detail {

    @SerializedName("compassDegrees")
    @Expose
    private Double compassDegrees;
    @SerializedName("endPathIndices")
    @Expose
    private List<Double> endPathIndices = null;
    @SerializedName("maneuverType")
    @Expose
    private String maneuverType;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("names")
    @Expose
    private List<String> names = null;
    @SerializedName("roadType")
    @Expose
    private String roadType;
    @SerializedName("startPathIndices")
    @Expose
    private List<Double> startPathIndices = null;

    public Double getCompassDegrees() {
        return compassDegrees;
    }

    public void setCompassDegrees(Double compassDegrees) {
        this.compassDegrees = compassDegrees;
    }

    public List<Double> getEndPathIndices() {
        return endPathIndices;
    }

    public void setEndPathIndices(List<Double> endPathIndices) {
        this.endPathIndices = endPathIndices;
    }

    public String getManeuverType() {
        return maneuverType;
    }

    public void setManeuverType(String maneuverType) {
        this.maneuverType = maneuverType;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }

    public String getRoadType() {
        return roadType;
    }

    public void setRoadType(String roadType) {
        this.roadType = roadType;
    }

    public List<Double> getStartPathIndices() {
        return startPathIndices;
    }

    public void setStartPathIndices(List<Double> startPathIndices) {
        this.startPathIndices = startPathIndices;
    }

}
