
package com.example.smartbussystem.BingDirectionApi;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BingDirectionApi {

    @SerializedName("traceId")
    @Expose
    private String traceId;
    @SerializedName("copyright")
    @Expose
    private String copyright;
    @SerializedName("statusDescription")
    @Expose
    private String statusDescription;
    @SerializedName("brandLogoUri")
    @Expose
    private String brandLogoUri;
    @SerializedName("resourceSets")
    @Expose
    private List<ResourceSet> resourceSets = null;
    @SerializedName("authenticationResultCode")
    @Expose
    private String authenticationResultCode;
    @SerializedName("statusCode")
    @Expose
    private Double statusCode;

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getBrandLogoUri() {
        return brandLogoUri;
    }

    public void setBrandLogoUri(String brandLogoUri) {
        this.brandLogoUri = brandLogoUri;
    }

    public List<ResourceSet> getResourceSets() {
        return resourceSets;
    }

    public void setResourceSets(List<ResourceSet> resourceSets) {
        this.resourceSets = resourceSets;
    }

    public String getAuthenticationResultCode() {
        return authenticationResultCode;
    }

    public void setAuthenticationResultCode(String authenticationResultCode) {
        this.authenticationResultCode = authenticationResultCode;
    }

    public Double getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Double statusCode) {
        this.statusCode = statusCode;
    }

}
