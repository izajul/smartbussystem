
package com.example.smartbussystem.BingDirectionApi;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Instruction {

    @SerializedName("maneuverType")
    @Expose
    private String maneuverType;
    @SerializedName("text")
    @Expose
    private String text;

    public String getManeuverType() {
        return maneuverType;
    }

    public void setManeuverType(String maneuverType) {
        this.maneuverType = maneuverType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
